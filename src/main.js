import * as VueGoogleMaps from 'vue2-google-maps';
import Vue from 'vue';
import VueRouter from 'vue-router';
import Index from './Index.vue';
require("./style.scss");

import App from './App.vue';
import Collections from "./collection/Collections.vue";
import NewCollection from "./collection/NewCollection.vue";
import VueResource from 'vue-resource';
import LoadCollection from "./collection/LoadCollection.vue";
import CollectionDetails from "./collection/CollectionDetails.vue";
import Classifier from "./classifier/Classifier.vue";
import ClassifierResults from "./classifier/ClassifierResults.vue"
import NewExecution from "./execution/NewExecution.vue"
import VeeValidate from "vee-validate";
import AlertMessage from './Componentes/AlertMessage.vue';
const config = {fieldsBagName:"valFields"}

Vue.use(VeeValidate,config);
Vue.use(VueRouter);
Vue.use(VueResource);
Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyBLVP1UVt2JQPQ_H1HdWn5Nw5tn3CU6XMc',
  }
});
Vue.mixin({
  data: function() {
    return {
      get getConstants() {
        return {
              /*
                  routes:{ convocatorias: "/convocatorias",
                         nuevaConvocatoria: "/convocatoria/nueva",
                         nuevaConfigurarCriterios: "/convocatoria/nueva-criterios",
                         nuevaConfigurarRelevancia: "/convocatoria/nueva-relevancia",
                         modificarResultados: "/convocatoria/modificar-resultados",
                         reclutados: "/convocatoria/nueva-reclutados"

                       },

                       */
                API_URL:"http://localhost:8082/clasificadores-desastres/"
              };
      },
      //currentCriteriaData:"55",
      //currentCriteriaRelevance:null,
    }

  }
})


const routes = [
  { path: '/index', alias: '/', component: Collections},
  {  path: '/collections',component: Collections},
  {  path: '/collection/new',component: NewCollection},
  {  path: '/collection/load',component: LoadCollection},
  {  path: '/collection/:id',component: CollectionDetails},
  {  path: '/collection/:collectionId/classifier/:classifierId',component:Classifier},
  {  path: '/collection/:collectionId/classifier/:classifierId/run/:runId',component:ClassifierResults},
  {  path: '/collection/:collectionId/classifier/:classifierId/execution/new',component: NewExecution},

,
]

// Create the router instance and pass the `routes` option
const router = new VueRouter({
  routes
})

Vue.component('alert-message', AlertMessage);
new Vue({
  el: '#app',
  router,
  render: h => h(App)
})
