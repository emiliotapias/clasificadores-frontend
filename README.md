# Front-end de plataforma de clasificadores

## Instalación

* Descargar dependencias con `npm install`
* Ejecutar modo de desarrollo con `npm run dev`
* Generar distribuible para producción `npm run build`
